//| * libquipu
//| #summary Provides core data structure for textual operations.
//| #dependencies @c/standard
//|

#include <stdio.h>
#include <stdint.h>

#define U_KNOT uintptr_t

#define NODE_TAIL_MASK     0    /* 000 */
#define PIVOT_TAIL_MASK    1    /* 001 */
#define HAIR_TAIL_MASK     2    /* 010 */
#define STRAND_TAIL_MASK   3    /* 011 */ 
/* Proposed */
#define QUIPU_LOCK_MASK    5    /* 101 */


//| ** Data structures

union Quipu ;

typedef struct Pivot {
	union Quipu * left  ;
	union Quipu * right ;
	U_KNOT len ;
	U_KNOT  reference ; 
} Pivot ;

typedef struct Node {
	union Quipu * left  ;
	union Quipu * right ;
	U_KNOT len ;
	void * semmantics ;
} Node ;

typedef struct Hair {
	char * str[31] ;
	char tag_byte ; // = flag value; 3?

} Hair ;

typedef struct Strand {
	union Quipu * left  ; // initialized w. customary value
	union Quipu * right ;
	U_KNOT len ;
	U_KNOT strand_v ; 
} Strand ;

typedef union  Quipu {
    Pivot  pivot   ;
    Node   node    ;
    Hair   hair    ;
    Strand strand  ;
} Quipu ; 


//| ** Discrimination of Quipu union


//| returns true if quipu is a Pivot
bool is_Pivot(qp) {

}

bool is_Node(qp) {

}

bool is_Hair(qp) {

}

bool is_Strand(qp) {


}

//| general discrimination function
//| returns the value of the mask stored
//| in at least 8 bits
//|
//| this function depends on the quipu being non-null.
uint_fast8_t qp_discriminate(Quipu qp) {

}


//| ** Linkers
//| Takes a quipu, and a pointer to another.
//| Links the pointer to the left side of the quipu.
//| Returns the quipu. 
Quipu qp_link_left (Quipu qp, Quipu * qptr) {

	return qp;
}

//| Takes a quipu, and a pointer to another.
//| Links the pointer to the right side of the quipu.
//| Returns the quipu. 
Quipu qp_link_right (Quipu qp, Quipu * qptr) {

	return qp;
}


void quip_hello() {
	printf("hi quipu.");
}