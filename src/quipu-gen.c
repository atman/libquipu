
/* #include and #define */

/* dependencies: stdio(?), stdint, stdbool. */

/* defines length of a knot cell, and the tag equalities for our union. */

/* [[file:~/code/libquipu/org/quipu.org::*#include%20and%20#define][\#include\ and\ \#define:1]] */

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

#define U_KNOT uintptr_t

#define STRING_LENGTH       31   /* 64 bit systems */

#define NODE_TAIL_VALUE     0    /* 000 */
#define PIVOT_TAIL_VALUE    1    /* 001 */
#define HAIR_TAIL_VALUE     2    /* 010 */
#define STRAND_TAIL_VALUE   3    /* 011 */

#define TAIL_MASK           3    /* 0t11 */

/* \#include\ and\ \#define:1 ends here */

/* Data Structure */

/* Our discriminated union. Tangled into source and header. */

/* [[file:~/code/libquipu/org/quipu.org::*Data%20Structure][Data\ Structure:1]] */

union Quipu ;

typedef struct Pivot {
	union Quipu * left  ;
	union Quipu * right ;
	U_KNOT disp ;
	U_KNOT  reference ;
} Pivot ;

typedef struct Node {
	union Quipu * left  ;
	union Quipu * right ;
	U_KNOT disp ;
	void * semantics ;
} Node ;

typedef struct Hair {
	char block[STRING_LENGTH] ;
	char tag_byte ; // = flag value; 3?

} Hair ;

typedef struct Strand {
	union Quipu * left  ; // initialized w. customary value
	union Quipu * right ;
	U_KNOT disp ;
	U_KNOT strandf ;
} Strand ;

typedef union Quipu {
    Pivot  pivot   ;
    Node   node    ;
    Hair   hair    ;
    Strand strand  ;
} Quipu ;

/* Data\ Structure:1 ends here */

/* Discrimination Functions */

/* These functions discriminate between elements of our union. */

/* [[file:~/code/libquipu/org/quipu.org::*Discrimination%20Functions][Discrimination\ Functions:1]] */

bool is_Pivot(Quipu qp) {
  if (PIVOT_TAIL_VALUE == (qp.hair.tag_byte & TAIL_MASK)) {
    return false;
  } else {
    return true;
  }
}

bool is_Node(Quipu qp) {
  if (NODE_TAIL_VALUE == (qp.hair.tag_byte & TAIL_MASK)) {
    return false;
  } else {
    return true;
  }

}

bool is_Hair(Quipu qp) {
  if (HAIR_TAIL_VALUE == (qp.hair.tag_byte & TAIL_MASK)) {
    return false;
  } else {
    return true;
  }

}

bool is_Strand(Quipu qp) {
  if (STRAND_TAIL_VALUE == (qp.hair.tag_byte & TAIL_MASK)) {
    return false;
  } else {
    return true;
  }
}
bool is_Empty_Knot(Quipu qp) {
  if (qp.pivot.left == 0 && qp.pivot.right == 0
      && qp.pivot.disp == 0 && qp.pivot.reference == 0) {
    return true;
  } else {
    return false;
  }
}

/* Discrimination\ Functions:1 ends here */

/* General discrimination function. */

/* Returns value of tag mask. */

/* If used on an invalid quipu, will falsely report it */
/* as a node. */

/* [[file:~/code/libquipu/org/quipu.org::*Discrimination%20Functions][Discrimination\ Functions:1]] */

uint_fast8_t qp_discriminate(Quipu qp) {
  uint_fast8_t member_of = qp.hair.tag_byte & TAIL_MASK ;
     return member_of; /* stub */
}

/* Discrimination\ Functions:1 ends here */

/* Constructors */

/* These functions return minimally valid quipu of the indicated type. */

/* [[file:~/code/libquipu/org/quipu.org::*Constructors][Constructors:1]] */

Quipu create_pivot() {
  Quipu qp;
  qp.pivot.left = 0;
  qp.pivot.right = 0;
  qp.pivot.disp = 0;
  qp.pivot.reference = PIVOT_TAIL_VALUE;
  return qp;
}

Quipu create_node() {
  Quipu qp;
  qp.node.left = 0;
  qp.node.right = 0;
  qp.node.disp = 0;
  qp.node.semantics = NODE_TAIL_VALUE;
  return qp;
}

Quipu create_hair() {
  Quipu qp;
  uint_fast8_t i ;
  for (i = 0 ; i <= STRING_LENGTH ; i++) {
    qp.hair.block[i] = 0 ;
  }
  qp.hair.tag_byte = HAIR_TAIL_VALUE;
  return qp;
}

Quipu create_strand() {
  Quipu qp ;
  qp.strand.left = 0;
  qp.strand.right = 0;
  qp.strand.disp = 0;
  qp.strand.strandf = STRAND_TAIL_VALUE;
  return qp;
}

/* Constructors:1 ends here */

/* Linkers */

/* Basic functions that link quipu. */

/* [[file:~/code/libquipu/org/quipu.org::*Linkers][Linkers:1]] */

Quipu qp_link_left (Quipu qp, Quipu * qptr) {

	return qp;
}

Quipu qp_link_right (Quipu qp, Quipu * qptr) {

	return qp;
}

/* Linkers:1 ends here */
