/* Headers for Quipu library */

#include <stdio.h>
#include <stdint.h>

#define U_KNOT uintptr_t
#define PIVOT_VAL 
#define PIVOT_SEM_PTR 93   // arbitrary
#define STRAND_SEM_PTR 42  // likewise

//| ** Data structures

union Quipu ;

typedef struct Pivot {
	union Quipu * left  ;
	union Quipu * right ;
	U_KNOT len ;
	U_KNOT  pivot_v ; 
} Pivot ;

typedef struct Node {
	union Quipu * left  ;
	union Quipu * right ;
	U_KNOT len ;
	void * sem_ptr ;
} Node ;

typedef struct Hair {
	char * str[31] ;
	char buffer ; // = flag value; 3?

} Hair ;

typedef struct Strand {
	union Quipu * left  ; // initialized w. customary value
	union Quipu * right ;
	U_KNOT len ;
	U_KNOT strand_v ; 
} Strand ;

typedef union  Quipu {
    Pivot  pivot   ;
    Node   node    ;
    Hair   hair    ;
    Strand strand  ;
} Quipu ; 

//| ** Atomic Operations


Quipu qp_add_left(Quipu qp, Quipu qp_l) {


}

Quipu qp_add_right(Quipu qp, Quipu qp_r) {

	
}